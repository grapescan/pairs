package me.grapescan.pairs.data;

import android.net.Uri;

import junit.framework.Assert;

import org.junit.Test;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import me.grapescan.pairs.game.Card;

public class CardRepositoryTest {

    @Test
    public void testCardLoading() throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);
        CardRepository repo = new CardRepository(new SimpleImageSource(), new SimpleUriStorage());
        repo.requestCardPairs(10, new CardRepository.Callback() {
            @Override
            public void onCardsPrepared(List<Card> cards) {
                Assert.assertTrue(cards.size() == 10);
                latch.countDown();
            }

            @Override
            public void onError() {
                Assert.assertTrue(false);
                latch.countDown();
            }
        });
        latch.await();
    }


}
