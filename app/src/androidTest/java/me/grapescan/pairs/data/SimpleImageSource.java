package me.grapescan.pairs.data;

import android.net.Uri;

import java.util.ArrayList;
import java.util.List;

public class SimpleImageSource implements ImageSource {
    private static final String[] URIS = {
            "http://cdn3-www.cattime.com/assets/uploads/2011/08/best-kitten-names-1.jpg",
            "https://www.pets4homes.co.uk/images/articles/1646/large/kitten-emergencies-signs-to-look-out-for-537479947ec1c.jpg",
            "https://pbs.twimg.com/profile_images/562466745340817408/_nIu8KHX.jpeg",
            "http://daysgoneby.me/wp-content/uploads/2014/09/gray-kitten.jpg",
            "http://vignette4.wikia.nocookie.net/happypasta/images/6/6c/Anime-kittens-cats-praying-496315.jpg/revision/latest?cb=20130914024839",
            "https://assets.rbl.ms/9241419/980x.jpg"
    };

    @Override
    public void getImageUris(int count, Callback callback) {
        final List<Uri> imageLinks = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            imageLinks.add(Uri.parse(URIS[i]));
        }
        callback.onImageUrisLoaded(imageLinks);
    }
}
