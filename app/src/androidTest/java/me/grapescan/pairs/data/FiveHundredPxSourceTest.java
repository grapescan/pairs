package me.grapescan.pairs.data;

import android.net.Uri;

import junit.framework.Assert;

import org.junit.Test;

import java.util.List;
import java.util.concurrent.CountDownLatch;

public class FiveHundredPxSourceTest {

    @Test
    public void testImageLoading() throws InterruptedException {
        final CountDownLatch latch = new CountDownLatch(1);
        FiveHundredPxSource src = new FiveHundredPxSource();
        src.getImageUris(10, new ImageSource.Callback() {
            @Override
            public void onImageUrisLoaded(List<Uri> uris) {
                Assert.assertTrue(uris.size() == 10);
                latch.countDown();
            }

            @Override
            public void onError() {
                Assert.fail();
                latch.countDown();
            }
        });
        latch.await();
    }


}
