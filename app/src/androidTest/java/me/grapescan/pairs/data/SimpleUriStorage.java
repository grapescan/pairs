package me.grapescan.pairs.data;

import android.net.Uri;

import java.util.List;

class SimpleUriStorage implements Storage<List<Uri>> {
    private List<Uri> uriList;

    @Override
    public List<Uri> load() {
        return uriList;
    }

    @Override
    public void save(List<Uri> data) {
        uriList = data;
    }
}