package me.grapescan.pairs;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.annotation.MainThread;
import android.support.v4.content.LocalBroadcastManager;

import java.util.concurrent.TimeUnit;

import me.grapescan.pairs.analytics.AppAnalytics;

@MainThread
public final class ApplicationLifecycleManager {
	public static final String ACTION_APPLICATION_BECOME_FOREGROUND = "ACTION_APPLICATION_BECOME_FOREGROUND";
	public static final String ACTION_APPLICATION_BECOME_BACKGROUND = "ACTION_APPLICATION_BECOME_BACKGROUND";
	public static final String ACTION_SESSION_TIMEOUT = "ACTION_SESSION_TIMEOUT";

	private static final long BACKGROUND_TIMEOUT = 300;
	private static final long SESSION_TIMEOUT = TimeUnit.MINUTES.toMillis(10);

	private Handler mainThreadHandler = new Handler(Looper.getMainLooper());

	//	Without a counter we consider that the app goes to background each time any activity is paused.
	//	It causes incorrect behavior if we have several activities in stack and configuration changes.
	private int countOfResumedActivities;
	private Context appContext;

	private static class SingletonHolder {
		public static ApplicationLifecycleManager instance = new ApplicationLifecycleManager();
	}

	private final Runnable backgroundTimer = new Runnable() {
		@Override
		public void run() {
			setForeground(false);
		}
	};

	private final Application.ActivityLifecycleCallbacks lifecycleCallbacks = new Application.ActivityLifecycleCallbacks() {
		@Override
		public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
		}

		@Override
		public void onActivityStarted(Activity activity) {
		}

		@Override
		public void onActivityResumed(Activity activity) {
			countOfResumedActivities++;
			if (countOfResumedActivities == 1) {
				setForeground(true);
			}
			mainThreadHandler.removeCallbacks(backgroundTimer);
		}

		@Override
		public void onActivityPaused(Activity activity) {
			countOfResumedActivities--;
			if (countOfResumedActivities == 0) {
				mainThreadHandler.postDelayed(backgroundTimer, BACKGROUND_TIMEOUT);
			}
		}

		@Override
		public void onActivityStopped(Activity activity) {
		}

		@Override
		public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
		}

		@Override
		public void onActivityDestroyed(Activity activity) {
		}
	};

	private final BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			onSessionTimeout();
		}
	};

	private volatile boolean foreground;

	private ApplicationLifecycleManager() {
	}

	public static ApplicationLifecycleManager getInstance() {
		return SingletonHolder.instance;
	}

	public void start(Context appContext) {
		this.appContext = appContext;
		((Application)appContext).registerActivityLifecycleCallbacks(lifecycleCallbacks);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            appContext.registerReceiver(receiver, new IntentFilter(ACTION_SESSION_TIMEOUT), Context.RECEIVER_NOT_EXPORTED);
        } else {
			appContext.registerReceiver(receiver, new IntentFilter(ACTION_SESSION_TIMEOUT));
		}
    }

	public boolean isForeground() {
		return foreground;
	}

	public boolean isBackground() {
		return !foreground;
	}

	private synchronized void setForeground(boolean foreground) {
		if (this.foreground != foreground) {
			this.foreground = foreground;
			if (foreground) {
				LocalBroadcastManager.getInstance(appContext).sendBroadcast(new Intent(ACTION_APPLICATION_BECOME_FOREGROUND));
				AppAnalytics.trackAppOpened();
				unscheduleInactivityAlarm();
			} else {
				LocalBroadcastManager.getInstance(appContext).sendBroadcast(new Intent(ACTION_APPLICATION_BECOME_BACKGROUND));
				AppAnalytics.trackAppClosed();
				scheduleInactivityAlarm();
			}
		}
	}

	private void scheduleInactivityAlarm() {
		Intent intent = new Intent(ACTION_SESSION_TIMEOUT);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(appContext, 0, intent, PendingIntent.FLAG_IMMUTABLE);

		AlarmManager alarmManager = (AlarmManager) appContext.getSystemService(Context.ALARM_SERVICE);
		if (alarmManager != null) {
			alarmManager.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + SESSION_TIMEOUT, pendingIntent);
		}
	}

	private void unscheduleInactivityAlarm() {
		Intent intent = new Intent(ACTION_SESSION_TIMEOUT);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(appContext, 0, intent, PendingIntent.FLAG_IMMUTABLE);

		AlarmManager alarmManager = (AlarmManager) appContext.getSystemService(Context.ALARM_SERVICE);
		if (alarmManager != null) {
			alarmManager.cancel(pendingIntent);
		}
	}

	private void onSessionTimeout() {
		AppAnalytics.trackAppTimeout();
		LocalBroadcastManager.getInstance(appContext).sendBroadcast(new Intent(ACTION_SESSION_TIMEOUT));
	}
}