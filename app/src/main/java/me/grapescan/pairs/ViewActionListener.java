package me.grapescan.pairs;

public interface ViewActionListener {
    void onComplete();
}
