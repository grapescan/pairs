package me.grapescan.pairs;

import java.util.concurrent.CountDownLatch;

public class CompositeViewActionListener implements ViewActionListener {
    private final ViewActionListener listener;
    private CountDownLatch latch;

    public CompositeViewActionListener(ViewActionListener listener, int length) {
        this.listener = listener;
        latch = new CountDownLatch(length);
    }

    @Override
    public void onComplete() {
        latch.countDown();
        if (latch.getCount() == 0) {
            listener.onComplete();
        }
    }
}
