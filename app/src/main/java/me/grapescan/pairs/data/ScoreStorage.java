package me.grapescan.pairs.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.grapescan.pairs.game.Score;
import me.grapescan.pairs.game.Type;

public class ScoreStorage implements Storage<Map<Type,List<Score>>> {
    private static final String SCORES = "scores";
    private final SharedPreferences prefs;
    private final Gson gson = new Gson();

    public ScoreStorage(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public Map<Type, List<Score>> load() {
        String scoresJson = prefs.getString(SCORES, null);
        if (scoresJson == null) {
            return new HashMap<>();
        } else {
            return gson.fromJson(scoresJson, new TypeToken<Map<Type,List<Score>>>() {}.getType());
        }
    }

    @Override
    public void save(Map<Type, List<Score>> data) {
        prefs.edit().putString(SCORES, gson.toJson(data)).commit();
    }
}
