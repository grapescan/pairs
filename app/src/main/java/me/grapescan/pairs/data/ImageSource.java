package me.grapescan.pairs.data;

import android.net.Uri;

import java.util.List;

public interface ImageSource {
    interface Callback {
        void onImageUrisLoaded(List<Uri> uris);
        void onError(Throwable error);
    }
    void getImageUris(int count, Callback callback);
}
