package me.grapescan.pairs.data;

import android.net.Uri;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public class FiveHundredPxSource implements ImageSource {

    private class SearchPhotosResultPage {
        class Photo {
            class Image {
                @SerializedName("url")
                String url;
            }

            @SerializedName("images")
            Image[] images;
        }

        @SerializedName("total_items")
        int totalItems;
        @SerializedName("photos")
        Photo[] photos;
    }

    public interface FiveHundredPxApi {
        @GET("v1/photos/search")
        Call<SearchPhotosResultPage> searchPhotos(@Query("consumer_key") String consumerKey, @Query("term") String term, @Query("page") int page);
    }

    @Override
    public void getImageUris(final int count, final Callback callback) {
        FiveHundredPxApi api = new Retrofit.Builder()
                .baseUrl("https://api.500px.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(FiveHundredPxApi.class);

        loadImageUris(api, 1, 0, count, new ArrayList<Uri>(count), callback);
    }

    private void loadImageUris(final FiveHundredPxApi api, final int page, final int offset, final int count, final List<Uri> uris, final Callback callback) {
        api.searchPhotos("YUhstzgwfMo1Sxff1OPeD4a4LAOp9EC3OBxPRptz", "kitten", page).enqueue(new retrofit2.Callback<SearchPhotosResultPage>() {
            @Override
            public void onResponse(Call<SearchPhotosResultPage> call, Response<SearchPhotosResultPage> response) {
                if (response == null) {
                    callback.onError(new Exception("Null response"));
                    return;
                }
                SearchPhotosResultPage searchResultPage = response.body();
                if (searchResultPage == null || searchResultPage.totalItems < count) {
                    callback.onError(new IllegalStateException("Not enough search results"));
                    return;
                }

                for (int i = 0; i < Math.min(searchResultPage.photos.length, count - offset); i++) {
                    uris.add(Uri.parse(searchResultPage.photos[i].images[0].url));
                }
                if (offset + searchResultPage.photos.length >= count) {
                    callback.onImageUrisLoaded(uris);
                } else {
                    loadImageUris(api, page + 1, offset + searchResultPage.totalItems, count, uris, callback);
                }
            }

            @Override
            public void onFailure(Call<SearchPhotosResultPage> call, Throwable t) {
                callback.onError(t);
            }
        });
    }
}
