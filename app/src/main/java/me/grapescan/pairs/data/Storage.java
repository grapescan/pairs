package me.grapescan.pairs.data;

import android.support.annotation.Nullable;

public interface Storage<T> {
    @Nullable T load();
    void save(T data);
}
