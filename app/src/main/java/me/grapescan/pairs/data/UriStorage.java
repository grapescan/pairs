package me.grapescan.pairs.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UriStorage implements Storage<List<Uri>> {
    private static final String IMAGE_URIS = "image_uris";
    private final SharedPreferences prefs;

    public UriStorage(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public List<Uri> load() {
        Set<String> uriStrings = prefs.getStringSet(IMAGE_URIS, Collections.<String>emptySet());
        List<Uri> uris = new ArrayList<Uri>(uriStrings.size());
        for (String uriString : uriStrings) {
            uris.add(Uri.parse(uriString));
        }
        return uris;
    }

    @Override
    public void save(List<Uri> data) {
        Set<String> uriStrings = new HashSet<>(data.size());
        for (Uri uri : data) {
            uriStrings.add(uri.toString());
        }
        prefs.edit().putStringSet(IMAGE_URIS, uriStrings).commit();
    }
}
