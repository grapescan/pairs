package me.grapescan.pairs.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import me.grapescan.pairs.game.Game;

public class GameStorage implements Storage<Game> {

    private static final String CURRENT_GAME = "current_game";

    private final SharedPreferences prefs;
    private Gson gson = new GsonBuilder()
            .registerTypeAdapter(Uri.class, new UriAdapter())
            .create();

    public GameStorage(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Override
    public Game load() {
        String gameData = prefs.getString(CURRENT_GAME, null);
        if (gameData == null) {
            return null;
        } else {
            return gson.fromJson(gameData, Game.class);
        }
    }

    @Override
    public void save(Game game) {
        String gameJson = gson.toJson(game);
        prefs.edit().putString(CURRENT_GAME, gameJson).commit();
    }

}
