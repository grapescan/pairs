package me.grapescan.pairs.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import me.grapescan.pairs.game.Score;
import me.grapescan.pairs.game.Type;

public class ScoreRepository {
    private static final int MAX_SCORES = 3;
    private Storage<Map<Type, List<Score>>> scoreStorage;

    public ScoreRepository(Storage<Map<Type,List<Score>>> scoreStorage) {
        this.scoreStorage = scoreStorage;
    }

    public void put(Score score) {
        Map<Type,List<Score>> scores = scoreStorage.load();
        putScore(scores, score);

        scoreStorage.save(scores);
    }

    private void putScore(Map<Type, List<Score>> scores, Score newScore) {
        List<Score> highScores = scores.get(newScore.getGameSpec().getGameType());
        if (highScores == null) {
            highScores = new ArrayList<>();
        }
        Iterator<Score> scoreIterator = highScores.iterator();
        while (scoreIterator.hasNext()) {
            Score score = scoreIterator.next();
            if (score.getValue() == newScore.getValue()) {
                scoreIterator.remove();
            }
        }
        highScores.add(newScore);
        Collections.sort(highScores);
        scores.put(newScore.getGameSpec().getGameType(), highScores.subList(0, Math.min(MAX_SCORES, highScores.size())));
    }

    public List<Score> get(Type gameType) {
        Map<Type,List<Score>> scores = scoreStorage.load();
        List<Score> highScores = scores.get(gameType);
        return highScores == null ? Collections.<Score>emptyList() : highScores;
    }

    public void reset() {
        scoreStorage.save(new HashMap<Type, List<Score>>());
    }
}
