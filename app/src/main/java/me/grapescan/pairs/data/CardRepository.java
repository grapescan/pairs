package me.grapescan.pairs.data;

import android.net.Uri;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import me.grapescan.pairs.analytics.CardsAnalytics;
import me.grapescan.pairs.game.Card;

public class CardRepository {
    public interface Callback {
        void onCardsPrepared(List<Card> cards);

        void onError();
    }

    private ImageSource imageSource;
    private Storage<List<Uri>> uriStorage;

    public CardRepository(ImageSource imageSource, Storage<List<Uri>> uriStorage) {
        this.imageSource = imageSource;
        this.uriStorage = uriStorage;
    }

    public void requestCardPairs(final int pairsCount, final Callback callback) {
        List<Uri> savedUris = uriStorage.load();
        if (savedUris != null && savedUris.size() >= pairsCount) {
            List<Card> cards = buildCards(pairsCount, savedUris);
            callback.onCardsPrepared(cards);
        } else {
            loadUris(callback, pairsCount);
        }

    }

    private List<Card> buildCards(int pairsCount, List<Uri> uris) {
        List<Card> cards = new ArrayList<>();
        for (int i = 0; i < pairsCount * 2; i++) {
            cards.add(new Card(i, uris.get(i / 2)));
        }
        Collections.shuffle(cards);
        return cards;
    }

    private void loadUris(final Callback callback, final int pairsCount) {
        Executors.newSingleThreadExecutor().submit(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                final long loadingStartMillis = System.currentTimeMillis();
                CardsAnalytics.trackLoadingStarted();
                imageSource.getImageUris(pairsCount, new ImageSource.Callback() {
                    @Override
                    public void onImageUrisLoaded(List<Uri> uris) {
                        CardsAnalytics.trackLoadingFinished(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - loadingStartMillis));

                        uriStorage.save(uris);
                        List<Card> cards = buildCards(pairsCount, uris);
                        callback.onCardsPrepared(cards);
                    }

                    @Override
                    public void onError(Throwable error) {
                        CardsAnalytics.trackLoadingFailed(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis() - loadingStartMillis));

                        callback.onError();
                    }
                });
                return null;
            }
        });
    }
}
