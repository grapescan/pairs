package me.grapescan.pairs;

import android.animation.Animator;

import me.grapescan.pairs.ViewActionListener;

public class AnimatorCompleteListener implements Animator.AnimatorListener {
    private ViewActionListener listener;

    public AnimatorCompleteListener(ViewActionListener listener) {
        this.listener = listener;
    }


    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {
        listener.onComplete();
    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }
}
