package me.grapescan.pairs.board;

import android.support.annotation.NonNull;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import me.grapescan.pairs.App;
import me.grapescan.pairs.R;
import me.grapescan.pairs.analytics.AppAnalytics;
import me.grapescan.pairs.analytics.GameAnalytics;
import me.grapescan.pairs.data.ScoreRepository;
import me.grapescan.pairs.data.Storage;
import me.grapescan.pairs.game.Card;
import me.grapescan.pairs.game.Game;
import me.grapescan.pairs.game.Listener;
import me.grapescan.pairs.game.Player;
import me.grapescan.pairs.game.Type;

public class BoardPresenter implements BoardContract.Presenter {
    private Game game;
    private WeakReference<BoardContract.View> viewRef = new WeakReference<>(null);

    private Storage<Game> gameStorage;
    private ScoreRepository scoreRepository;
    private final Listener gameStateListener = new Listener() {
        @Override
        public void onCardFlip(final Card card) {
            getView().showCardFlip(card);
        }

        @Override
        public void onCardsMismatch(final Card... cards) {
            getView().showCardsMismatched(cards);
        }

        @Override
        public void onCardsMatch(final Card... cards) {
            getView().showCardsMatched(cards);
        }

        @Override
        public void onGameStart(final List<Card> cards) {
            GameAnalytics.trackGameStarted(game.getType());
            getView().showCards(cards);
        }

        @Override
        public void onGameFinish(final Game game) {
            GameAnalytics.trackGameFinished(game.getElapsedTime(), game.getType());
            scoreRepository.put(game.getBestScore());
            getView().showCards(game.getCards());
            getView().showLeaderBoard(game.getScores());
            getView().close();
        }

        @Override
        public void onCardsUnflip(final Card... cards) {
            BoardContract.View view = viewRef.get();
            view.showUnflip(cards);
        }

        @Override
        public void onGameLoadingError() {
            // TODO: Show error message
        }

        @Override
        public void onScoreChange(Player player) {
            getView().showScore(player);
        }

        @Override
        public void onTimerUpdate(long elapsedTimeSeconds) {
            getView().showTimer(formatTime(elapsedTimeSeconds));
        }

        @Override
        public void onPlayerSwitch(Player player) {
            getView().showPlayerActive(player);
        }
    };

    private String formatTime(long elapsedTimeSeconds) {
        String timeString = String.format(Locale.ENGLISH, "%02d:%02d",
                TimeUnit.SECONDS.toMinutes(elapsedTimeSeconds) % TimeUnit.HOURS.toMinutes(1),
                TimeUnit.SECONDS.toSeconds(elapsedTimeSeconds) % TimeUnit.MINUTES.toSeconds(1));
        if (elapsedTimeSeconds > TimeUnit.HOURS.toSeconds(1)) {
            timeString = String.format(Locale.ENGLISH, "%02d:%02d:%02d", TimeUnit.SECONDS.toHours(elapsedTimeSeconds),
                    TimeUnit.SECONDS.toMinutes(elapsedTimeSeconds) % TimeUnit.HOURS.toMinutes(1),
                    TimeUnit.SECONDS.toSeconds(elapsedTimeSeconds) % TimeUnit.MINUTES.toSeconds(1));
        }
        return timeString;
    }

    public BoardPresenter(Storage<Game> gameStorage, ScoreRepository scoreRepository) {
        this.gameStorage = gameStorage;
        this.scoreRepository = scoreRepository;
    }

    @NonNull
    private BoardContract.View getView() {
        BoardContract.View view = viewRef.get();
        return view == null ? BoardContract.VIEW_STUB : view;
    }

    private void initView(Type gameType) {
        getView().showTitle(getTitle(gameType));
        switch (gameType) {
            case TIME_ATTACK_SOLITAIRE:
                getView().setTimerVisible(true);
                getView().showTimer(formatTime(0));
                break;
            default:
                for (Player player : game.getPlayers()) {
                    getView().showScore(player);
                }
                if (game.getPlayers().length > 1) {
                    getView().showPlayerActive(game.getCurrentPlayer());
                }
                getView().setTimerVisible(false);
                break;
        }
    }

    private String getTitle(Type gameType) {
        switch (gameType) {
            case PENALTY_SOLITAIRE:
                return App.Companion.getStringRes(R.string.board_title_penalty_solitaire);
            case TIME_ATTACK_SOLITAIRE:
                return App.Companion.getStringRes(R.string.board_title_time_attack_solitaire);
            case DUEL:
                return App.Companion.getStringRes(R.string.board_title_duel);
            default:
                throw new RuntimeException("Unknown game type: " + gameType);
        }
    }

    @Override
    public void onViewAttach(BoardContract.View view) {
        AppAnalytics.trackAppBrowse("board");

        viewRef = new WeakReference<>(view);
        game = gameStorage.load();
        game.setListener(gameStateListener);
        if (!game.isStarted()) {
            game.setStarted(true);
        }
        initView(game.getType());
        view.showCards(game.getCards());
        // TODO: show timer, score, etc
    }

    @Override
    public void onCardClick(final Card card) {
        game.select(card);
    }

    @Override
    public void onCloseClick() {
        game.pause();
        game = null;
        gameStorage.save(null);
        getView().close();
    }

    @Override
    public void onViewDetach() {
        if (game != null) {
            game.pause();
        }
        gameStorage.save(game);
    }
}
