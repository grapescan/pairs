package me.grapescan.pairs.board;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;

import me.grapescan.pairs.AnimatorCompleteListener;
import me.grapescan.pairs.R;
import me.grapescan.pairs.ViewActionListener;
import me.grapescan.pairs.game.Card;

class CardViewHolder extends RecyclerView.ViewHolder {
    private final AnimatorSet cardFlipOutAnimation;
    private final AnimatorSet cardFlipInAnimation;
    private final AnimatorSet cardFadeAwayAnimation;
    private CardAdapter.OnCardClickListener clickListener;
    private ImageView cardFront;
    private ImageView cardBack;
    private final Transformation roundedCornersTransformation;

    public CardViewHolder(View itemView, CardAdapter.OnCardClickListener clickListener) {
        super(itemView);
        this.roundedCornersTransformation = new RoundedCornersTransformation(itemView.getContext(), 12, 0);
        this.clickListener = clickListener;
        this.cardFront = (ImageView) itemView.findViewById(R.id.card_front);
        this.cardBack = (ImageView) itemView.findViewById(R.id.card_back);
        cardFlipOutAnimation = (AnimatorSet) AnimatorInflater.loadAnimator(itemView.getContext(), R.animator.card_flip_out_animation);
        cardFlipInAnimation = (AnimatorSet) AnimatorInflater.loadAnimator(itemView.getContext(), R.animator.card_flip_in_animation);
        cardFadeAwayAnimation = (AnimatorSet) AnimatorInflater.loadAnimator(itemView.getContext(), R.animator.card_fade_away_animation);
    }

    public void bind(final Card card) {
        itemView.setVisibility(card.isMatched() ? View.GONE : View.VISIBLE);
        changeCameraDistance();
        Glide.with(itemView.getContext())
                .load(card.getImageUri())
                .transform(new RoundedCornersTransformation(itemView.getContext(), 12, 0))
                .into(cardFront);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onClick(card);
            }
        });
    }

    private void changeCameraDistance() {
        int distance = 8000;
        float scale = itemView.getContext().getResources().getDisplayMetrics().density * distance;
        cardFront.setCameraDistance(scale);
        cardBack.setCameraDistance(scale);
    }

    public void flip(final ViewActionListener listener) {
        cardFlipOutAnimation.setTarget(cardBack);
        cardFlipInAnimation.setTarget(cardFront);
        cardFlipOutAnimation.addListener(new AnimatorCompleteListener(listener));
        cardFlipOutAnimation.start();
        cardFlipInAnimation.start();
    }

    public void match(final ViewActionListener listener) {
        cardFadeAwayAnimation.setTarget(cardFront);
        cardFadeAwayAnimation.addListener(new AnimatorCompleteListener(listener));
        cardFadeAwayAnimation.start();
    }

    public void unflip(final ViewActionListener listener) {
        cardFlipOutAnimation.setTarget(cardFront);
        cardFlipInAnimation.setTarget(cardBack);
        cardFlipOutAnimation.addListener(new AnimatorCompleteListener(listener));
        cardFlipOutAnimation.start();
        cardFlipInAnimation.start();
    }
}
