package me.grapescan.pairs.board;

import android.os.Handler;
import android.os.Looper;

import java.util.LinkedList;
import java.util.List;

import me.grapescan.pairs.ViewActionListener;

public class ViewActionQueue implements ViewActionListener {
    private final Handler uiHandler = new Handler(Looper.getMainLooper());
    private List<Runnable> actions = new LinkedList<Runnable>();
    private boolean isActionRunning = false;

    private void executeNextAction() {
        isActionRunning = true;
        Runnable nextAction = actions.get(0);
        actions.remove(nextAction);
        uiHandler.post(nextAction);
    }

    public void submit(Runnable runnable) {
        actions.add(runnable);
        if (!isActionRunning) {
            executeNextAction();
        }
    }

    @Override
    public void onComplete() {
        if (actions.isEmpty()) {
            isActionRunning = false;
        } else {
            executeNextAction();
        }
    }
}
