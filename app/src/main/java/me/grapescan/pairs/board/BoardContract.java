package me.grapescan.pairs.board;

import java.util.List;

import me.grapescan.pairs.game.Card;
import me.grapescan.pairs.game.Player;
import me.grapescan.pairs.game.Score;

public class BoardContract {
    interface View {
        void showCards(List<Card> cards);

        void showCardFlip(Card card);

        void showUnflip(Card... cards);

        void showCardsMatched(Card... cards);

        void showCardsMismatched(Card... card);

        void showScore(Player player);

        void showTimer(String timeString);

        void showLeaderBoard(List<Score> scores);

        void close();

        void setTimerVisible(boolean isVisible);

        void showTitle(String title);

        void showPlayerActive(Player player);
    }

    public interface Presenter {
        void onViewAttach(View view);

        void onCardClick(Card card);

        void onCloseClick();

        void onViewDetach();
    }

    public static final View VIEW_STUB = new View() {
        @Override
        public void showCards(List<Card> cards) {

        }

        @Override
        public void showCardFlip(Card card) {

        }

        @Override
        public void showUnflip(Card... cards) {

        }

        @Override
        public void showCardsMatched(Card... cards) {

        }

        @Override
        public void showCardsMismatched(Card... card) {

        }

        @Override
        public void showScore(Player player) {

        }

        @Override
        public void showTimer(String timeString) {

        }

        @Override
        public void showLeaderBoard(List<Score> scores) {

        }

        @Override
        public void close() {

        }

        @Override
        public void setTimerVisible(boolean isVisible) {

        }

        @Override
        public void showTitle(String title) {

        }

        @Override
        public void showPlayerActive(Player player) {

        }
    };
}
