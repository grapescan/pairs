package me.grapescan.pairs.board;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import me.grapescan.pairs.R;
import me.grapescan.pairs.game.Card;

public class CardAdapter extends RecyclerView.Adapter<CardViewHolder> {
    private OnCardClickListener cardClickListener;

    public CardAdapter(OnCardClickListener cardClickListener) {
        this.cardClickListener = cardClickListener;
    }

    public int getPosition(Card card) {
        return cards.indexOf(card);
    }

    interface OnCardClickListener {
        void onClick(Card card);
    }

    private List<Card> cards = new ArrayList<>();

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cardView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card, parent, false);
        return new CardViewHolder(cardView, cardClickListener);
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, int position) {
        holder.bind(cards.get(position));
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
        notifyDataSetChanged();
    }
}
