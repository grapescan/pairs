package me.grapescan.pairs.board;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.ColorInt;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.grapescan.pairs.App;
import me.grapescan.pairs.CompositeViewActionListener;
import me.grapescan.pairs.R;
import me.grapescan.pairs.ViewActionListener;
import me.grapescan.pairs.game.Card;
import me.grapescan.pairs.game.Player;
import me.grapescan.pairs.game.Score;
import me.grapescan.pairs.game.Type;
import me.grapescan.pairs.scores.ScoresActivity;

public class BoardActivity extends AppCompatActivity implements BoardContract.View {
    private static final String TAG = BoardActivity.class.getSimpleName();
    private static final String EXTRA_GAME_TYPE = "game_type";

    public static void start(Context context, Type gameType) {
        Intent starter = new Intent(context, BoardActivity.class);
        starter.putExtra(EXTRA_GAME_TYPE, gameType);
        context.startActivity(starter);
    }

    private final Handler uiHandler = new Handler(Looper.getMainLooper());
    private final CardAdapter.OnCardClickListener cardClickListener = new CardAdapter.OnCardClickListener() {
        @Override
        public void onClick(Card card) {
            presenter.onCardClick(card);
        }
    };
    private final CardAdapter adapter = new CardAdapter(cardClickListener);
    private RecyclerView grid;
    private View close;
    private TextView title;
    private TextView score1;
    private TextView score2;
    private TextView timer;
    private int primaryColor;
    private int primaryDarkColor;
    private BoardContract.Presenter presenter;
    private Map<Player,TextView> scoreViews = new HashMap<Player,TextView>();
    private ViewActionQueue actionQueue = new ViewActionQueue();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_board);
        primaryColor = getColorAttrValue(android.support.v7.appcompat.R.attr.colorPrimary);
        primaryDarkColor = getColorAttrValue(android.support.v7.appcompat.R.attr.colorPrimaryDark);
        title = (TextView) findViewById(R.id.board_title);
        close = findViewById(R.id.board_close);
        score1 = (TextView) findViewById(R.id.score1);
        score2 = (TextView) findViewById(R.id.score2);
        timer = (TextView) findViewById(R.id.timer);
        initGrid();
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onCloseClick();
            }
        });

        presenter = App.Presenters.INSTANCE.getBoardPresenter();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.onViewAttach(this);
        scoreViews = new HashMap<>();
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onViewDetach();
    }

    private int getColorAttrValue(int attrId) {
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[] { attrId };
        TypedArray a = obtainStyledAttributes(typedValue.data, textSizeAttr);
        int resId = a.getColor(0, -1);
        a.recycle();
        return resId;
    }

    private void initGrid() {
        grid = (RecyclerView) findViewById(R.id.cards_grid);
        grid.setHasFixedSize(true);
        grid.setAdapter(adapter);
        grid.setLayoutManager(new GridLayoutManager(this, getSpanCount()));
        grid.setItemAnimator(new DefaultItemAnimator());
    }

    private int getSpanCount() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int cardSize = getResources().getDimensionPixelSize(R.dimen.board_card_size);
        int padding = getResources().getDimensionPixelSize(R.dimen.board_padding);
        return (displayMetrics.widthPixels - 2 * padding) / cardSize;
    }

    @Override
    public void showCards(final List<Card> cards) {
        actionQueue.submit(new Runnable() {
            @Override
            public void run() {
                adapter.setCards(cards);
            }
        });
        actionQueue.onComplete();
    }

    @Override
    public void showCardFlip(final Card card) {
        actionQueue.submit(new Runnable() {
            @Override
            public void run() {
                int position = adapter.getPosition(card);
                CardViewHolder cardViewHolder = (CardViewHolder) grid.findViewHolderForAdapterPosition(position);
                cardViewHolder.flip(actionQueue);
            }
        });
    }

    @Override
    public void showUnflip(final Card... cards) {
        actionQueue.submit(new Runnable() {
            @Override
            public void run() {
                ViewActionListener compositeListener = new CompositeViewActionListener(actionQueue, cards.length);
                for (Card card : cards) {
                    int position = adapter.getPosition(card);
                    CardViewHolder cardViewHolder = (CardViewHolder) grid.findViewHolderForAdapterPosition(position);
                    cardViewHolder.unflip(compositeListener);
                }
            }
        });
    }

    @Override
    public void showCardsMatched(final Card... cards) {
        actionQueue.submit(new Runnable() {
            @Override
            public void run() {
                ViewActionListener compositeListener = new CompositeViewActionListener(actionQueue, cards.length);
                for (Card card : cards) {
                    showCardMatch(card, compositeListener);
                }
            }
        });
    }

    private void showCardMatch(Card card, final ViewActionListener listener) {
        Log.d(TAG, "showCardMatch() called with: card = [" + card + "], listener = [" + listener + "]");
        int position = adapter.getPosition(card);
        CardViewHolder cardViewHolder = (CardViewHolder) grid.findViewHolderForAdapterPosition(position);
        cardViewHolder.match(listener);
    }

    @Override
    public void showCardsMismatched(Card... cards) {
        actionQueue.submit(new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        actionQueue.onComplete();
                    }
                }, 300);
            }
        });
    }

    @Override
    public void showScore(Player player) {
        TextView scoreView = getScoreView(player);
        scoreView.setVisibility(View.VISIBLE);
        scoreView.setText(String.valueOf(player.getScore()));
    }

    private TextView getScoreView(Player player) {
        if (scoreViews.containsKey(player)) {
            return scoreViews.get(player);
        } else {
            if (scoreViews.containsValue(score1)) {
                if (scoreViews.containsValue(score2)) {
                    throw new RuntimeException("Too many players");
                } else {
                    scoreViews.put(player, score2);
                    return score2;
                }
            } else {
                scoreViews.put(player, score1);
                return score1;
            }
        }
    }

    @Override
    public void showTimer(final String timeString) {
        uiHandler.post(new Runnable() {
            @Override
            public void run() {
                timer.setText(timeString);
            }
        });
    }

    @Override
    public void showLeaderBoard(List<Score> scores) {
        ScoresActivity.Companion.start(this, scores);
    }

    @Override
    public void close() {
        finish();
    }

    @Override
    public void setTimerVisible(boolean isVisible) {
        timer.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showTitle(String titleString) {
        title.setText(titleString);
    }

    @Override
    public void showPlayerActive(Player activePlayer) {
        for (View scoreView : scoreViews.values()) {
            scoreView.setBackgroundResource(R.drawable.board_score_highlight);
        }
        getScoreView(activePlayer).setBackgroundColor(primaryColor);
    }
}
