package me.grapescan.pairs

import android.app.Application
import android.content.Context

import me.grapescan.pairs.board.BoardContract

import me.grapescan.pairs.board.BoardPresenter
import me.grapescan.pairs.data.CardRepository
import me.grapescan.pairs.data.GameStorage
import me.grapescan.pairs.data.LocalImageSource
import me.grapescan.pairs.data.ScoreRepository
import me.grapescan.pairs.data.ScoreStorage
import me.grapescan.pairs.data.UriStorage
import me.grapescan.pairs.game.Score
import me.grapescan.pairs.menu.MenuPresenter
import me.grapescan.pairs.scores.ScoresContract
import me.grapescan.pairs.scores.ScoresPresenter

class App : Application() {

    object Presenters {
        val boardPresenter: BoardContract.Presenter by lazy {
            BoardPresenter(Data.gameStorage, Repository.scoreRepository)
        }
        private val scoresPresenterInternal: ScoresContract.Presenter by lazy<ScoresContract.Presenter> {
            ScoresPresenter(Repository.scoreRepository, Repository.cardRepository, Data.gameStorage)
        }
        fun getScoresPresenter(scores: List<Score>) : ScoresContract.Presenter {
            scoresPresenterInternal.setScores(scores)
            return scoresPresenterInternal
        }
        val menuPresenter: MenuPresenter by lazy {
            MenuPresenter(Data.gameStorage, Repository.cardRepository, Repository.scoreRepository)
        }
    }

    private object Repository {
        val cardRepository: CardRepository by lazy {
            CardRepository(LocalImageSource(), UriStorage(appContext))
        }
        val scoreRepository: ScoreRepository by lazy {
            ScoreRepository(ScoreStorage(appContext))
        }
    }

    private object Data {
        val gameStorage: GameStorage by lazy {
            GameStorage(appContext)
        }
    }

    override fun onCreate() {
        super.onCreate()
        appContext = applicationContext
        ApplicationLifecycleManager.getInstance().start(appContext)
    }

    companion object {
        lateinit var appContext: Context
            private set

        fun getStringRes(resId: Int): String {
            return appContext.getString(resId)
        }
    }
}
