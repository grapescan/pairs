package me.grapescan.pairs

import java.lang.ref.WeakReference
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class WeakRefDelegate<T,R> : ReadWriteProperty<R, T?> {

    var viewRef: WeakReference<T?> = WeakReference(null)

    override fun getValue(thisRef: R, property: KProperty<*>): T? {
        return viewRef.get()
    }

    override fun setValue(thisRef: R, property: KProperty<*>, value: T?) {
        viewRef = WeakReference(value)
    }
}