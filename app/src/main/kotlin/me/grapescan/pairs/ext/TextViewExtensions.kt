package me.grapescan.pairs.ext

import android.content.Context
import android.os.Build
import android.widget.TextView

fun TextView.applyTextAppearance(context: Context, styleResId: Int) {
    if (Build.VERSION.SDK_INT < 23) {
        setTextAppearance(context, styleResId)
    } else {
        setTextAppearance(styleResId)
    }
}