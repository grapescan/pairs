package me.grapescan.pairs.game

enum class Type {
    PENALTY_SOLITAIRE, TIME_ATTACK_SOLITAIRE, DUEL
}
