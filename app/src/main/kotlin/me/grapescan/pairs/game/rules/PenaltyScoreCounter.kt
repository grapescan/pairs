package me.grapescan.pairs.game.rules

import me.grapescan.pairs.game.*

class PenaltyScoreCounter : Rule {
    override fun isApplicable(game: Game): Boolean = true

    override fun apply(game: Game, listener: Listener?) {
        game.getCurrentPlayer().score += 10
        listener?.onScoreChange(game.getCurrentPlayer())
    }

}
