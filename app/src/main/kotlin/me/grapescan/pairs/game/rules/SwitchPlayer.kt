package me.grapescan.pairs.game.rules

import me.grapescan.pairs.game.*

class SwitchPlayer : Rule {
    override fun isApplicable(game: Game): Boolean = true

    override fun apply(game: Game, listener: Listener?) {
        val iter = game.players.iterator()
        var stop : Boolean = false
        while (!stop && iter.hasNext()) {
            val player = iter.next()
            if (player.isActive) {
                player.isActive = false
                val nextPlayerIndex = (game.players.indexOf(player) + 1) % game.players.size
                game.players[nextPlayerIndex].isActive = true
                listener?.onPlayerSwitch(game.players[nextPlayerIndex])
                stop = true
            }
        }
    }
}
