package me.grapescan.pairs.game.rules

import me.grapescan.pairs.game.*

class DetectMatchingPair : Rule {

    override fun isApplicable(game: Game): Boolean {
        with(game.cards.filter { it.isFlipped }, {
            return size == 2 && get(0).matches(get(1))
        })
    }

    override fun apply(game: Game, listener: Listener?) {
        val flippedCards : Array<Card> = game.cards.filter { it.isFlipped }.toTypedArray()

        flippedCards.forEach {
            it.isFlipped = false
            it.isMatched = true
        }

        listener?.onCardsMatch(*flippedCards)
    }
}
