package me.grapescan.pairs.game.rules

import me.grapescan.pairs.game.*

class EndGame : Rule {
    override fun isApplicable(game: Game): Boolean {
        return !game.isFinished && game.cards.all { it.isMatched }
    }

    override fun apply(game: Game, listener: Listener?) {
        game.isFinished = true
        listener?.onGameFinish(game)
    }
}
