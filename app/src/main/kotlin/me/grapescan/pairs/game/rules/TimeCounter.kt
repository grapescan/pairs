package me.grapescan.pairs.game.rules

import me.grapescan.pairs.game.*
import java.util.Timer
import java.util.TimerTask

class TimeCounter : Rule {
    @Transient private var timerTask: TimerTask? = null
    @Transient private var timer: Timer? = null
    private var isActive: Boolean = false

    override fun isApplicable(game: Game): Boolean {
        return shouldStart(game) || shouldStop(game)
    }

    override fun apply(game: Game, listener: Listener?) {
        if (shouldStart(game)) {
            startTimer(game)
            isActive = true
        }
        if (shouldStop(game)) {
            stopTimer()
            isActive = false
        }
    }

    private fun shouldStop(game: Game): Boolean {
        return (!game.isStarted || game.isFinished) && isActive
    }

    private fun shouldStart(game: Game): Boolean {
        return game.isStarted && !game.isFinished && !isActive
    }

    private fun stopTimer() {
        timerTask!!.cancel()
    }

    private fun startTimer(game: Game) {
        if (timer == null) {
            timer = Timer()
        }
        timerTask = object : TimerTask() {
            override fun run() {
                game.elapsedTime++
                game.players.forEach { it.score = game.elapsedTime.toInt() }
            }
        }
        timer!!.scheduleAtFixedRate(timerTask, 0, 1000)
    }
}
