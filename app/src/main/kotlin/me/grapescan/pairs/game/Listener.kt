package me.grapescan.pairs.game

interface Listener {
    fun onCardFlip(card: Card)

    fun onCardsMismatch(vararg cards: Card)

    fun onCardsMatch(vararg cards: Card)

    fun onGameStart(cards: List<Card>)

    fun onGameFinish(game: Game)

    fun onCardsUnflip(vararg card: Card)

    fun onGameLoadingError()

    fun onScoreChange(player: Player)

    fun onTimerUpdate(elapsedTimeSeconds: Long)

    fun onPlayerSwitch(player: Player)
}