package me.grapescan.pairs.game

class Score(val gameId: Long, val gameSpec: Spec, val value: Long, val player: Player) : Comparable<Score> {

    override fun compareTo(o: Score): Int {
        return if (value < o.value) -1 else if (value == o.value) 0 else 1
    }

    override fun equals(obj: Any?): Boolean {
        if (obj is Score) {
            val score = obj as Score?
            return gameId == score!!.gameId
        } else {
            return false
        }
    }
}
