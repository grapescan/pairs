package me.grapescan.pairs.game.rules

import me.grapescan.pairs.game.*

class Sequence(val rules: Array<Rule>) : Rule {

    override fun isApplicable(game: Game): Boolean = rules[0].isApplicable(game)

    override fun apply(game: Game, listener: Listener?) {
        var stop: Boolean = false
        rules.forEach {
            if (!stop && it.isApplicable(game)) {
                it.apply(game, listener)
            } else {
                stop = true
            }
        }
    }
}
