package me.grapescan.pairs.game

interface Rule {
    fun isApplicable(game: Game): Boolean
    fun apply(game: Game, listener: Listener?)
}
