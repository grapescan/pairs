package me.grapescan.pairs.game

import android.util.Log

import java.util.ArrayList

import me.grapescan.pairs.data.CardRepository

class Game(val id: Long, private val spec: Spec, val cards: List<Card>) {
    interface Callback {
        fun onGameCreated(game: Game)
        fun onError()
    }

    companion object {
        private val TAG = Game::class.java.simpleName


        fun create(spec: Spec, cardRepository: CardRepository, callback: Callback) {
            cardRepository.requestCardPairs(6, object : CardRepository.Callback {
                override fun onCardsPrepared(cards: List<Card>) {
                    val game = Game(System.currentTimeMillis(), spec, cards)
                    callback.onGameCreated(game)
                }

                override fun onError() {
                    callback.onError()
                }
            })
        }
    }

    @Transient var listener: Listener? = null
    val players: Array<Player> = spec.players
    val type: Type
        get() = spec.gameType

    var isFinished: Boolean = false
        set(finished) {
            field = finished
            if (finished) {
                listener?.onGameFinish(this)
            }
        }

    var isStarted = true
        set(started) {
            field = started
            if (started) {
                listener?.onGameStart(cards)
            }
            applyRules()
        }

    var elapsedTime: Long = 0
        set(elapsedTime) {
            field = elapsedTime
            listener?.onTimerUpdate(this.elapsedTime)
        }

    fun select(card : Card) {
        card.isSelected = true
        applyRules()
    }

    private fun applyRules() {
        var rulesApplied: Boolean
        do {
            rulesApplied = false
            for (rule in spec.rules) {
                if (rule.isApplicable(this)) {
                    Log.d(TAG, "Applying rule: " + rule.javaClass.simpleName)
                    rule.apply(this, listener)
                    rulesApplied = true
                }
            }
        } while (rulesApplied)
    }



    fun pause() {
        isStarted = false
        applyRules()
    }

    fun getScores(): List<Score> {
        val scoreList = ArrayList<Score>(players.size)
        for (i in players.indices) {
            scoreList.add(Score(id, spec, players[i].score.toLong(), players[i]))
        }
        return scoreList
    }

    fun getBestScore() : Score {
        return spec.scoreCalculator.invoke(getScores())!!
    }

    fun getCurrentPlayer() : Player {
        with(players.find { it.isActive }) {
            if (this == null) {
                players[0].isActive = true
                return players[0]
            } else {
                return this
            }
        }
    }
}
