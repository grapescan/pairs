package me.grapescan.pairs.game

import android.net.Uri

class Card(val id: Long, uri: Uri) {
    val imageUri: Uri = uri
    var isFlipped: Boolean = false
    var isMatched: Boolean = false
    var isSelected: Boolean = false

    override fun toString(): String {
        return "Card id=$id, flipped=$isFlipped, matched=$isMatched, selected=$isSelected"
    }

    fun matches(card: Card): Boolean {
        return imageUri == card.imageUri
    }
}
