package me.grapescan.pairs.game

import me.grapescan.pairs.game.rules.*

enum class Spec(val gameType: Type, val players: Array<Player>, val rules: Array<Rule>, val scoreCalculator: (List<Score>) -> Score?) {
    PENALTY_SOLITAIRE(Type.PENALTY_SOLITAIRE,
            arrayOf(Player("Player")),
            arrayOf(FlipSelectedCard(),
                    DetectMatchingPair(),
                    Sequence(arrayOf(
                            DetectMismatchingPair(),
                            PenaltyScoreCounter()
                    )),
                    EndGame()),
            { it.min() }),

    TIME_ATTACK_SOLITAIRE(Type.TIME_ATTACK_SOLITAIRE,
            arrayOf(Player("Player")),
            arrayOf(FlipSelectedCard(),
                    DetectMatchingPair(),
                    DetectMismatchingPair(),
                    TimeCounter(),
                    EndGame()),
            { it.min() }),

    DUEL(Type.DUEL,
            arrayOf(Player("Player 1"), Player("Player 2")),
            arrayOf(FlipSelectedCard(),
                    DetectMatchingPair(),
                    Sequence(arrayOf(
                            DetectMismatchingPair(),
                            PenaltyScoreCounter(),
                            SwitchPlayer()
                    )),
                    EndGame()),
            { it.min() })
}
