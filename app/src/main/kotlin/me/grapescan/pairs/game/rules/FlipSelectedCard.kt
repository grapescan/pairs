package me.grapescan.pairs.game.rules

import me.grapescan.pairs.game.*

class FlipSelectedCard : Rule {
    override fun isApplicable(game: Game): Boolean = game.cards.find { it.isSelected } != null

    override fun apply(game: Game, listener: Listener?) {
        with(game.cards.find { it.isSelected }!!) {
            if (!isMatched && !isFlipped && game.cards.count { it.isFlipped } < 2) {
                isFlipped = true
                listener?.onCardFlip(this)
            }
            isSelected = false
        }
    }
}
