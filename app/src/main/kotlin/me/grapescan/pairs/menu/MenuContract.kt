package me.grapescan.pairs.menu

import me.grapescan.pairs.BasePresenter
import me.grapescan.pairs.game.Type

object MenuContract {
    interface View {
        fun showMenu()

        fun showBoard(currentGameType: Type)

        fun showScoresResetDone()

        fun showLoadingError()

        fun showLoadingProgress()
    }

    abstract class Presenter : BasePresenter<View>() {
        abstract fun onPenaltySolitaireClick()

        abstract fun onTimeAttackSolitaireClick()

        abstract fun onDuelClick()

        abstract fun onResetScoresClick()

        abstract fun onRetryClick()
    }
}
