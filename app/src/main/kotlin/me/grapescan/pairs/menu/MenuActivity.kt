package me.grapescan.pairs.menu

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast

import me.grapescan.pairs.App
import me.grapescan.pairs.R
import me.grapescan.pairs.board.BoardActivity
import me.grapescan.pairs.databinding.ActivityMenuBinding
import me.grapescan.pairs.ext.*
import me.grapescan.pairs.game.Type

class MenuActivity : AppCompatActivity(), MenuContract.View {

    private val presenter: MenuContract.Presenter by lazy {
        App.Presenters.menuPresenter
    }
    lateinit var binding: ActivityMenuBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.content.itemPenaltySolitaire.setOnClickListener { presenter.onPenaltySolitaireClick() }
        binding.content.itemTimeAttackSolitaire.setOnClickListener { presenter.onTimeAttackSolitaireClick() }
        binding.content.itemDuel.setOnClickListener { presenter.onDuelClick() }
        binding.content.menuResetScores.setOnClickListener { presenter.onResetScoresClick() }
        binding.menuRetry.setOnClickListener { presenter.onRetryClick() }

        presenter.onViewAttach(this)
    }

    override fun showMenu() {
        binding.menuContent.show()
        binding.menuError.hide()
        binding.menuProgress.hide()
    }

    override fun showBoard(currentGameType: Type) {
        BoardActivity.start(this, currentGameType)
    }

    override fun showScoresResetDone() {
        Toast.makeText(this, R.string.done, Toast.LENGTH_SHORT).show()
    }

    override fun showLoadingError() {
        binding.menuContent.hide()
        binding.menuError.show()
        binding.menuProgress.hide()
    }

    override fun showLoadingProgress() {
        binding.menuContent.hide()
        binding.menuError.hide()
        binding.menuProgress.show()
    }
}
