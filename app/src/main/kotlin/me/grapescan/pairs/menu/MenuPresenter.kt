package me.grapescan.pairs.menu

import me.grapescan.pairs.analytics.AppAnalytics
import me.grapescan.pairs.data.CardRepository
import me.grapescan.pairs.data.ScoreRepository
import me.grapescan.pairs.data.Storage
import me.grapescan.pairs.game.Spec
import me.grapescan.pairs.game.Game

class MenuPresenter(private val gameStorage: Storage<Game>, private val cardRepository: CardRepository, private val scoreRepository: ScoreRepository) : MenuContract.Presenter() {

    private var spec: Spec? = null
    private var isLoadingGame: Boolean = false
    private var isError: Boolean = false

    override fun onViewAttach(view: MenuContract.View) {
        super.onViewAttach(view)
        AppAnalytics.trackAppBrowse("menu")
        val currentGame = try { gameStorage.load() } catch (e: Exception) {null}
        if (isLoadingGame) {
            view.showLoadingProgress()
        } else if (isError) {
            view.showLoadingError()
        } else {
            if (currentGame != null && !currentGame.isFinished) {
                view.showBoard(currentGame.type)
            } else {
                view.showMenu()
            }
        }
    }

    private fun startNewGame(spec: Spec?) {
        if (spec == null) {
            return
        }
        this.spec = spec
        isLoadingGame = true
        view { showLoadingProgress() }
        Game.create(spec, cardRepository, object : Game.Callback {
            override fun onGameCreated(game: Game) {
                isLoadingGame = false
                isError = false
                gameStorage.save(game)
                view {
                    showMenu()
                    showBoard(game.type)
                }
            }

            override fun onError() {
                isLoadingGame = false
                isError = true
                view { showLoadingError() }
            }
        })
    }

    override fun onViewDetach() {}

    override fun onPenaltySolitaireClick() = startNewGame(Spec.PENALTY_SOLITAIRE)

    override fun onTimeAttackSolitaireClick() = startNewGame(Spec.TIME_ATTACK_SOLITAIRE)

    override fun onDuelClick() = startNewGame(Spec.DUEL)

    override fun onResetScoresClick() {
        scoreRepository.reset()
        view { showScoresResetDone() }
    }

    override fun onRetryClick() = startNewGame(spec)
}
