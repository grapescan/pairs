package me.grapescan.pairs

open class BasePresenter<T> {
    private var view: T? by WeakRefDelegate()

    fun view(func : T.() -> Unit) : Unit {
        this.view?.func()
    }

    open fun onViewAttach(view: T) {
        this.view = view
    }

    open fun onViewDetach() {

    }
}