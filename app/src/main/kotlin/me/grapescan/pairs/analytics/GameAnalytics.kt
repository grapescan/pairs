package me.grapescan.pairs.analytics

import android.util.Log
import me.grapescan.pairs.game.Type

class GameAnalytics {
    companion object {
        @JvmStatic
        fun trackGameStarted(type: Type) {
            Log.d("DMSO", "GameAnalytics.trackGameStarted(type = [$type])")
        }

        @JvmStatic
        fun trackGameFinished(elapsedTime: Long, type: Type) {
            Log.d("DMSO", "GameAnalytics.trackGameFinished(elapsedTime = [$elapsedTime], type = [$type])")
        }
    }
}