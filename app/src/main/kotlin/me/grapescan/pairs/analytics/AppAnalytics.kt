package me.grapescan.pairs.analytics

import android.util.Log

class AppAnalytics {
    companion object {
        @JvmStatic
        fun trackAppOpened() {
            Log.d("DMSO", "AppAnalytics.trackAppOpened()")
        }

        @JvmStatic
        fun trackAppClosed() {
            Log.d("DMSO", "AppAnalytics.trackAppClosed()")
        }

        @JvmStatic
        fun trackAppTimeout() {
            Log.d("DMSO", "AppAnalytics.trackAppTimeout()")
        }

        @JvmStatic
        fun trackAppBrowse(screen: String) {
            Log.d("DMSO", "AppAnalytics.trackAppBrowse(screen = [$screen])")
        }
    }
}