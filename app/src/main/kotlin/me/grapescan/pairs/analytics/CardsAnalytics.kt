package me.grapescan.pairs.analytics

import android.util.Log

class CardsAnalytics {
    companion object {
        @JvmStatic
        fun trackLoadingStarted() {
            Log.d("DMSO", "CardsAnalytics.trackLoadingStarted()")
        }

        @JvmStatic
        fun trackLoadingFinished(seconds: Long) {
            Log.d("DMSO", "CardsAnalytics.trackLoadingFinished(seconds = [$seconds])")
        }

        @JvmStatic
        fun trackLoadingFailed(seconds: Long) {
            Log.d("DMSO", "CardsAnalytics.trackLoadingFailed(seconds = [$seconds])")
        }
    }
}