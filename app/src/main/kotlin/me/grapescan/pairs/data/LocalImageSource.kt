package me.grapescan.pairs.data

import android.net.Uri
import me.grapescan.pairs.R

class LocalImageSource : ImageSource {

    private val resources = listOf(
        R.drawable.kitten1,
        R.drawable.kitten2,
        R.drawable.kitten3,
        R.drawable.kitten4,
        R.drawable.kitten5,
        R.drawable.kitten6,
        R.drawable.kitten7,
        R.drawable.kitten8,
        R.drawable.kitten9,
        R.drawable.kitten10,
        R.drawable.kitten11,
        R.drawable.kitten12,
        R.drawable.kitten13
    )

    override fun getImageUris(count: Int, callback: ImageSource.Callback?) {
        callback?.onImageUrisLoaded(resources.take(count)
                                        .map { resId ->
                                            Uri.parse("android.resource://me.grapescan.pairs/$resId")
                                        })
    }
}