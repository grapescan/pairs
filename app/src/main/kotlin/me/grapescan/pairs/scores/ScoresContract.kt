package me.grapescan.pairs.scores

import me.grapescan.pairs.BasePresenter
import me.grapescan.pairs.game.Score
import me.grapescan.pairs.game.Type

object ScoresContract {
    interface View {
        fun showScores(scores: Array<String>)

        fun highlightScore(index: Int)

        fun showGoldGoblet()

        fun showSilverGoblet()

        fun showBronzeGoblet()

        fun hideGoblet()

        fun close()

        fun showCongratulationText(text: String)

        fun hideCongratulationTitle()

        fun hideCongratulationText()

        fun showCongratulationTitle()

        fun showBoard(gameType: Type)

        fun showPlayAgain()

        fun hidePlayAgain()

        fun showWreath()
    }

    abstract class Presenter : BasePresenter<View>() {
        abstract fun onCloseClick()

        abstract fun setScores(scores: List<Score>)

        abstract fun onPlayAgainClick()
    }
}

