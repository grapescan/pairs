package me.grapescan.pairs.scores

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

import java.util.ArrayList

import me.grapescan.pairs.App
import me.grapescan.pairs.R
import me.grapescan.pairs.board.BoardActivity
import me.grapescan.pairs.databinding.ActivityScoresBinding
import me.grapescan.pairs.ext.applyTextAppearance
import me.grapescan.pairs.ext.hide
import me.grapescan.pairs.ext.show
import me.grapescan.pairs.game.Score
import me.grapescan.pairs.game.Type

class ScoresActivity : AppCompatActivity(), ScoresContract.View {
    private val scoreViews: Array<TextView> by lazy {
        arrayOf<TextView>(binding.scoreList!!.itemScore1, binding.scoreList!!.itemScore2, binding.scoreList!!.itemScore3)
    }

    private val presenter: ScoresContract.Presenter by lazy {
        val scores : List<Score> = gson.fromJson(intent.getStringExtra(EXTRA_SCORE), object : TypeToken<ArrayList<Score>>() {}.type)
        App.Presenters.getScoresPresenter(scores)
    }

    private lateinit var binding: ActivityScoresBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityScoresBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.scoresClose.setOnClickListener { presenter.onCloseClick() }
        binding.playAgain.setOnClickListener { presenter.onPlayAgainClick() }
    }

    override fun onStart() {
        super.onStart()
        presenter.onViewAttach(this)
    }

    override fun showScores(scores: Array<String>) {
        for (i in scoreViews.indices) {
            if (scores.size > i) {
                scoreViews[i].text = scores[i]
                scoreViews[i].show()
            } else {
                scoreViews[i].hide()
            }
        }
        highlightScore(-1)
    }

    override fun highlightScore(index: Int) {
        val targetView = if (index >= 0 && index < scoreViews.size) scoreViews[index] else null
        for (scoreView in scoreViews) {
            scoreView.applyTextAppearance(this, if (scoreView == targetView) R.style.HighlightedScore else R.style.RegularScore)
        }
    }

    override fun showGoldGoblet() = showGoblet(R.drawable.scores_goblet_gold)

    override fun showSilverGoblet() = showGoblet(R.drawable.scores_goblet_silver)

    override fun showBronzeGoblet() = showGoblet(R.drawable.scores_goblet_bronze)

    private fun showGoblet(drawableResId: Int) {
        binding.scorePrize!!.prize.show()
        binding.scorePrize!!.prize.setImageResource(drawableResId)
    }

    override fun hideGoblet() = binding.scorePrize!!.prize.hide()

    override fun showCongratulationText(text: String) {
        binding.scorePrize!!.congratulationText.text = text
        binding.scorePrize!!.congratulationText.show()
    }

    override fun hideCongratulationTitle() = binding.scorePrize!!.congratulationTitle.hide()

    override fun hideCongratulationText() = binding.scorePrize!!.congratulationText.hide()

    override fun showCongratulationTitle() = binding.scorePrize!!.congratulationTitle.show()

    override fun close() = finish()

    override fun showBoard(gameType: Type) {
        BoardActivity.start(this, gameType)
        finish()
    }

    override fun showPlayAgain() = binding.playAgain.show()

    override fun hidePlayAgain() = binding.playAgain.hide()

    override fun showWreath() {
        binding.scorePrize!!.prize.show()
        binding.scorePrize!!.prize.setImageResource(R.drawable.scores_wreath)
    }

    companion object {

        private val EXTRA_SCORE = "score"
        private val gson = Gson()

        fun start(context: Context, scores: List<Score>) {
            val starter = Intent(context, ScoresActivity::class.java)
            starter.putExtra(EXTRA_SCORE, gson.toJson(scores))
            context.startActivity(starter)
        }
    }
}
