package me.grapescan.pairs.scores

import java.util.concurrent.TimeUnit

import me.grapescan.pairs.App
import me.grapescan.pairs.R
import me.grapescan.pairs.analytics.AppAnalytics
import me.grapescan.pairs.data.CardRepository
import me.grapescan.pairs.data.ScoreRepository
import me.grapescan.pairs.data.Storage
import me.grapescan.pairs.game.Score
import me.grapescan.pairs.game.Type
import me.grapescan.pairs.game.Game

class ScoresPresenter(private val scoreRepository: ScoreRepository, private val cardRepository: CardRepository, private val gameStorage: Storage<Game>) : ScoresContract.Presenter() {
    private var currentScore: List<Score> = emptyList()
    private var topScores: List<Score> = emptyList()

    private val drawText = App.getStringRes(R.string.score_duel_draw_text);
    private val winnerText = App.getStringRes(R.string.score_duel_winner_text)
    private val youAreFirst = App.getStringRes(R.string.scores_you_are_the_first)
    private val youAreSecond = App.getStringRes(R.string.scores_you_are_the_second)
    private val youAreThird = App.getStringRes(R.string.scores_you_are_the_third)
    private val notInTop = App.getStringRes(R.string.scores_you_are_not_in_top)

    override fun onViewAttach(view: ScoresContract.View) {
        super.onViewAttach(view)
        AppAnalytics.trackAppBrowse("scores")
        if (currentScore[0].gameSpec.gameType == Type.DUEL) {
            showWinner()
        } else {
            showTopScores()
        }
    }

    private fun showTopScores() {
        val position = topScores.indexOf(currentScore[0])
        view {
            showScores(topScores.map{ getScoreString(it) }.toTypedArray())
            highlightScore(position)
            showCongratulationTitle()
            showPlayAgain()
        }
        when (position) {
            0 -> {
                view {
                    showGoldGoblet()
                    showCongratulationText(youAreFirst)
                    highlightScore(0)
                }
            }
            1 -> {
                view {
                    showSilverGoblet()
                    showCongratulationText(youAreSecond)
                    highlightScore(1)
                }
            }
            2 -> {
                view {
                    showBronzeGoblet()
                    showCongratulationText(youAreThird)
                    highlightScore(2)
                }
            }
            else -> {
                view {
                    hideGoblet()
                    showCongratulationText(notInTop)
                }
            }
        }
    }

    private fun showWinner() {
        view {
            hideGoblet()
            showWreath()
            showScores(arrayOf(getScoreString(currentScore[0]) + " : " + getScoreString(currentScore[1])))
            highlightScore(0)
            showCongratulationText(if (isDraw()) drawText else String.format(winnerText, currentScore[0].player.name))
            showPlayAgain()
        }
    }

    private fun isDraw() : Boolean = currentScore.size > 1 && currentScore[0].value == currentScore[1].value

    override fun setScores(scores: List<Score>) {
        currentScore = scores
        topScores = scoreRepository.get(currentScore[0].gameSpec.gameType).sorted()
    }

    private fun getScoreString(score: Score): String {
        when (score.gameSpec.gameType) {
            Type.TIME_ATTACK_SOLITAIRE -> return formatTime(score.value)
            else -> return score.value.toString()
        }
    }

    private fun formatTime(elapsedTimeSeconds: Long): String {
        return if (elapsedTimeSeconds > TimeUnit.HOURS.toSeconds(1))
            "%02d:%02d:%02d".format(TimeUnit.SECONDS.toHours(elapsedTimeSeconds),
                    TimeUnit.SECONDS.toMinutes(elapsedTimeSeconds) % TimeUnit.HOURS.toMinutes(1),
                    TimeUnit.SECONDS.toSeconds(elapsedTimeSeconds) % TimeUnit.MINUTES.toSeconds(1))
            else "%02d:%02d".format(TimeUnit.SECONDS.toMinutes(elapsedTimeSeconds) % TimeUnit.HOURS.toMinutes(1),
                    TimeUnit.SECONDS.toSeconds(elapsedTimeSeconds) % TimeUnit.MINUTES.toSeconds(1))
    }

    override fun onCloseClick() = view { close() }

    override fun onPlayAgainClick() {
        Game.create(currentScore[0].gameSpec, cardRepository, object : Game.Callback {
            override fun onGameCreated(game: Game) {
                gameStorage.save(game)
                view { showBoard(game.type) }
            }

            override fun onError() {
                // TODO: handle error
            }
        })
    }
}
